from C_gestion_cartes import *
from B_initialisation_course import *

######### SECTION : DEPLACEMENT DES COUREURS #################
##############################################################

#Cette fonction détermine le nom du coureur qui est pour l'instant le plus loin
#du start dans la course.
#Elle prend en entrée le dictionnaire des positions
def le_plus_eloigne(dico_positions):
    max_case = -1
    epsilon = 0.5 #pour avantager les coureurs placés à droite
    max_joueur = ""
    for joueur in dico_positions:
        case = numero_de_case(joueur,dico_positions)
        if dico_positions[joueur][0] == "D":
            case += epsilon
        if case > max_case:
            max_joueur = joueur
            max_case = case
    return max_joueur

#Cette fonction détermine le nom du coureur qui est pour l'instant le plus
#en retrait dans la course.
#Elle prend en entrée le dictionnaire des positions et la liste des positions droite
def le_moins_eloigne(dico_positions,positions_droite):
    min_case = len(positions_droite)
    epsilon = 0.5 #pour avantager les coureurs placés à droite
    min_joueur = ""
    for joueur in dico_positions:
        case = numero_de_case(joueur,dico_positions)
        if dico_positions[joueur][0] == "D":
            case += epsilon
        if case < min_case:
            min_joueur = joueur
            min_case = case
    return min_joueur

#A chaque tour, le premier coureur à se déplacer est celui qui est en tête de
#la course, c'est-à-dire situé sur la plus haute case possible et à droite
#Cette fonction renvoie la liste des coureurs par ordre de placement sur la course
#Le coureur le plus proche de l'arrivée est situé en premier
def tri_joueurs(dico_positions):
    copie_dico = dict(dico_positions)
    ordre_joueurs = []
    while len(copie_dico) > 0:
        prochain_joueur = le_plus_eloigne(copie_dico)
        ordre_joueurs.append(prochain_joueur)
        del copie_dico[prochain_joueur]
    return ordre_joueurs