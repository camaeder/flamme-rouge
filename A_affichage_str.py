
######### SECTION : AFFICHAGE DU JEU ##########
###############################################

#Fonction ligne_vers_droite
#Pour représenter une ligne de route, où les coureurs se déplacent vers la droite
#positions est une liste contenant des caractères à afficher




#Fonction route_vers_droite
#Une route est constituée de deux lignes en parallèle, pour représenter les deux voies.
#Chaque case est constituée de deux côtés, droite et gauche.
#positions_droite et positions_gauche sont des listes de même longueur contenant des caractères




#Fonction inverse
#On souhaite maintenant créer des routes, où les coureurs se dirigent vers la gauche
#Cette fonction inverse une ligne, en enlevant le dernier élément et le remplaçant par "<"
def inverse(ligne):
    return ligne[:0:-1] + "<"



#Fonction route_vers_gauche
#Cette fonction renvoie une route où les coureurs se dirigent vers la gauche.




#Fonction ligne_depart
#Les routes de départ sont forcément vers la droite par défaut.
#Elles commencent par ">>>>>" au lieu de ">"
#Une zone de start est présente au début et termine avec un "X" au lieu de "|"




#Fonction route_depart
#Cette fonction représente une route de départ avec deux lignes de départ en parallèle




#Fonction ligne_arrivee_vers_droite
#On représente désormais une ligne d'arrivée où les coureurs se dirigent vers la droite
#Un entier terminus > 1 indique l'emplacement de la ligne d'arrivee pour gagner la course




#Fonction route_arrivee_vers_droite
##Cette fonction représente une route de départ avec
#deux lignes d'arrivee en parallèle


#Fonction route_arrivee_vers_gauche
#Même chose que route_arrivee_vers_droite sauf que cette fois_ci
#les coureurs se déplacent vers la gauche




#Fonction affichage_route
#Etant donné un nombre de lignes, la longueur de chaque ligne (taille_seg),
#deux liste de positions (pour la droite et la gauche),
#on affiche la route avec les caractéristiques indiquées et le contenu des listes
#de positions. L'emplacement du départ sur la première ligne (start) et de l'arrivée
#sur la dernière (terminus) sont donnés comme arguments optionnels
#On suppose nb_lignes >= 2




