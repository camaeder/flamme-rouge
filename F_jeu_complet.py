from A_affichage_str import *
from E_phase_terminale import *
import tkinter as tk

#Affichage de l'entete du jeu dans la console
#Les arguments optionnels peuvent être modifiés pour
#personnaliser son étape !
def entete(numero_etape = 12, depart = "Aurillac", arrivee = "Villeneuve-sur-Lot"):
    chaine = "#" * 100 + "\n"
    chaine += "#"*41 + " LA FLAMME ROUGE " + "#"*41 + "\n"
    chaine += "#" * 100 + "\n" + "\n"
    chaine += "Bienvenue sur l'étape " + str(numero_etape) + " "
    chaine += "du Tour de France 2024 reliant " + depart + " "
    chaine += "à " + arrivee + " ! Bonne chance !!!"
    chaine += "\n" + "\n"
    return chaine

#Affichage de la route dans une fenêtre graphique
def afficher_fenetre(chaine, taille_fen = '600x300+50+10'):
    fen = tk.Tk()
    fen.geometry(taille_fen)
    fen.title("La Flamme Rouge")
    bouton = tk.Button(fen, text="Tour suivant", command=fen.destroy)
    tk.Label(fen, text=chaine).pack()
    bouton.pack(side=tk.LEFT, padx=5, pady=5)
    fen.mainloop()