from random import *

######### SECTION : GESTION DES CARTES #################
########################################################

#Cette fonction donne le paquet initial détenu par chaque joueur
def paquet_initial():
    # ce paquet est initialement prévu pour une partie à 5 lignes,
    # des segments de 20 cases, un start = 4 et terminus = 10
    return [2] * 3 + [3] * 3 + [4] * 3 + [5] * 3 + [6] * 3 + [9] * 3

#Il va falloir stocker tout au long de la partie le paquet de cartes de chaque joueur
#Pour cela, on va manipuler un dictionnaire qui admettra les noms des joueurs
#comme clés et leur paquet comme valeur.
#Au début de la partie, les joueurs auront des paquets équivalents, mais ceux-ci évolueront
#au fur et à mesure de la partie, en fonction des déplacements de chacun.
#Cette fonction prend en entrée la liste des joueurs et renvoie le dictionnaire.
def dico_paquets_initial(liste_joueurs):
    dico = {}
    # A COMPLETER POUR REMPLIR LE DICTIONNAIRE
    return dico

#Chaque joueur possède aussi une défausse contenant les cartes déplacements
#qu'il n'a pas choisi. Lorsque la défausse sera vide, on la mélange et remet son contenu
#dans le paquet. Au début de la partie, la défausse est vide.
#Cette fonction crée donc un dictionnaire où les clés sont les joueurs et les valeurs
#sont les défausses initiales (à savoir des listes vides)
def dico_defausses_initial(liste_joueurs):
    dico = {}
    for joueur in liste_joueurs:
        dico[joueur] = []
    return dico


